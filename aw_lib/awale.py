#!/usr/bin/python3
# Filename: [awale.py]

################################################################################
##[ IMPORTS ]###################################################################
################################################################################

import sys;
import os;

def printf(format, *args):
	sys.stdout.write(format % args);

################################################################################
###[ CLASSE ]###################################################################
################################################################################

##################################################
class c_awale: ###################################
    """
        Awale main class which contains every variables
        and methods to play awale.
    """
    
    ##################################################
    ###[ PROPERTIES ]#################################
    ##################################################
    board = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    score = [0, 0];
    turn = 0;
    over = False;

    ##################################################
    ###[ METHODS ]####################################
    ##################################################

    ##############################
    def	init(self):
        # Init game: score, turn, board
        self.score[0] = 0;
        self.score[1] = 0;
        self.turn = 0;
        self.state = False;
        for cell in range(0, 12):
            self.board[cell] = 4;

    ##############################	
    ###[ PRINT ]################## 
    ##############################	

    ##############################
    def print_board(self):
        #os.system("clear || cls");
        printf("(%-2d)    ", self.score[1]);
        for i in range(6, 0, -1):
            printf("%d   ", i);
        printf("\n       ");
        for i in range(11, 5, -1):
            printf("[%-2d]", self.board[i]);
        if (self.turn == 1):
            printf(" <\n       ");
        else:
            printf("\n       ");
        for i in range(0, 6):
            printf("[%-2d]", self.board[i]);
        if (self.turn == 0):
            printf(" <\n");
        else:
            printf("\n");
        printf("(%-2d)    ", self.score[0]);
        for i in range(1, 7):
            printf("%d   ", i);
        printf("\n");

    ##############################
    def	get_turn(self):
        # Returns player current turn
        return (self.turn + 1);

    ##############################
    def	get_board(self, player):
        if (player == 1):
            return (self.board[:]);
        return (self.board[6:12] + self.board[0:6]);

    ##############################
    def	get_winner(self):
        if (self.score[0] > self.score[1]):
            return (1);
        return (2);

    ##############################
    def	get_score(self):
        return (self.score);

    ##############################	
    ###[ PLAY ]################### 
    ##############################		

    ##############################
    def deploy_peas(self, board, cell_origin, peas):
        cell = cell_origin;
        board[cell] = 0;
        for _ in range(0, peas):
            cell += 1;
            if (cell % 12 == cell_origin):
                cell += 1;
            board[cell % 12] += 1;
        return (cell % 12);

    ##############################
    def recolt_peas(self, board, cell, peas):
        recolt_score = 0;
        while (cell // 6 != self.turn):
            if (2 <= board[cell] <= 3):
                recolt_score += board[cell];
                board[cell] = 0;
                cell -= 1;
            else:
                break;
            if (cell < 0):
                cell = 11;
        self.score[self.turn] += recolt_score;

    ##############################
    def	game_over(self, winner):
        peas = 0;
        for i in range(winner * 6, winner * 6 + 6):
            peas += self.board[i];
        self.score[winner] += peas;
        self.over = True;

    ##############################
    def check_game_over(self):
        state = True;
        for i in range(0, 6):
            if (self.board[i] != 0):
                state = False;
                break;
        if (state == True):
            #self.game_over(1);
            return (0);
        state = True;
        for i in range(6, 12):
            if (self.board[i] != 0):
                state = False;
                break;
        if (state == True):
            #self.game_over(0);
            return (0);
        return (1);

    ##############################
    def play(self, cell):
        if (cell < 1 or cell > 6):
            return (1);
        cell = self.turn * 6 + (cell - 1);
        peas = self.board[cell];
        if (peas == 0):
            return (1);
        new_board = self.board[:];
        cell = self.deploy_peas(new_board, cell, peas);
        self.recolt_peas(new_board, cell, peas);
        self.board = new_board;
        if (self.turn == 0):
            self.turn = 1;
        elif (self.turn == 1):
            self.turn = 0;
        if (self.check_game_over() == 0):
            return (2);
        return (0);
