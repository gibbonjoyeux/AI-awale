#!/usr/bin/python3

################################################################################
##[ MODULES ]###################################################################
################################################################################

##################################################
##[ EXTERN ]######################################
##################################################

import random as rand;
import math;
import sys;

##################################################
##[ INTERN ]######################################
##################################################

from nn_lib     import neural as nn;
from ga_lib     import ga;
from aw_lib     import awale;

################################################################################
##[ DEFINES ]###################################################################
################################################################################

MAX_TURN        = 80;
POPULATION      = 5000;
GENERATION      = 1000;
SELECTION       = 20;
SELECTION_MODE  = "WHEEL"
LAYERS          = [12, 6, 6, 6, 6];
CHANCE_WEIGHT   = 1;
CHANCE_WEIGHT_2 = 5;
CHANCE_BIAS     = 2;

################################################################################
##[ FUNCTIONS ]#################################################################
################################################################################

##################################################
##[ MAIN FUNCTIONS ]##############################
##################################################

##############################
def printf(format, *args):
        sys.stdout.write(format % args)

##############################
def print_wave(generation, score):
    printf("[%3s] (%2d) [\033[31;01m%s\033[0m]\n", generation, score, "#" * score);

##############################
def sort_move(output):
    sorted = [];
    for i in range(0, 6):
        sorted.append({"order": i, "value": output[i]});
    sorted.sort(key=lambda x: x["value"], reverse=True);
    return (sorted);

##############################
def play(game, player_1, player_2):
    players = [player_1, player_2];
    game.init();
    turn = 0;
    while (turn < MAX_TURN):
        player = game.get_turn();
        player_board = game.get_board(player);
        for i in range(0, 12):
            player_board[i] = player_board[i] / 48;
        output = players[player - 1].launch(player_board);
        sorted = sort_move(output);
        for i in range(0, 6):
            state = game.play(sorted[i]["order"] + 1);
            if (state == 0):
                break;
            elif (state == 2):
                return (turn);
        turn += 1;
    return (MAX_TURN);

##################################################
##[ GENETIC ALGORITHM FUNCTIONS ]#################
##################################################

##############################
def genetic_birth_slice(elem_a, elem_b):
    def mix_layer(elem_a, elem_b):
        half = len(elem_a[0]["weights"]) // 2;
        new_layer = [];
        i = 0;
        ##### FOR EACH NEURON #####
        for i in range(0, len(elem_a)):
            part_a = elem_a[i]["weights"][0:half];
            part_b = elem_b[i]["weights"][half:];
            bias = elem_a[i]["bias"];
            new_neuron = {"bias": bias, "weights": part_a + part_b};
            new_layer.append(new_neuron);
        return (new_layer);
    elem_a = elem_a["nn"].get_net();
    elem_b = elem_b["nn"].get_net();
    child = [];
    child.append(elem_a[0]);
    for layer in range(1, len(elem_a)):
        child.append(mix_layer(elem_a[layer], elem_b[layer]));
    new_nn = nn.c_nn();
    new_nn.init_from_weights(child);
    return ({"note": 0, "nn": new_nn});

##############################
def genetic_birth_mean(elem_a, elem_b):
    elem_a = elem_a["nn"].get_net();
    elem_b = elem_b["nn"].get_net();
    child = [];
    child.append(elem_a[0]);
    ##### FOR EACH LAYER #####
    for i in range(1, len(elem_a)):
        new_layer = [];
        child.append(new_layer);
        ##### FOR EACH NEURON #####
        for j in range(0, len(elem_a[i])):
            weights = [];
            bias = (elem_a[i][j]["bias"] + elem_b[i][j]["bias"]) / 2;
            new_neuron = {"bias": bias, "weights": weights};
            new_layer.append(new_neuron);
            ##### FOR EACH WEIGHT #####
            for k in range(0, len(elem_a[i][j]["weights"])):
                weight_a = elem_a[i][j]["weights"][k];
                weight_b = elem_b[i][j]["weights"][k];
                new_weight = (weight_a + weight_b) / 2;
                weights.append(new_weight);
    new_nn = nn.c_nn();
    new_nn.init_from_weights(child);
    return ({"note": 0, "nn": new_nn});

##############################
def genetic_mutation_weight(elem):
    nn = elem["nn"].get_net();
    layer = math.floor(rand.uniform(1, len(nn)));
    neuron = math.floor(rand.uniform(0, len(nn[layer])));
    weight = math.floor(rand.uniform(0, len(nn[layer][neuron]["weights"])));
    nn[layer][neuron]["weights"][weight] += rand.uniform(-1, 1);
    return (elem);

##############################
def genetic_mutation_weight_2(elem):
    nn = elem["nn"].get_net();
    layer = math.floor(rand.uniform(1, len(nn)));
    neuron = math.floor(rand.uniform(0, len(nn[layer])));
    weight = math.floor(rand.uniform(0, len(nn[layer][neuron]["weights"])));
    nn[layer][neuron]["weights"][weight] = rand.uniform(-8, 8);
    return (elem);

##############################
def genetic_mutation_bias(elem):
    nn = elem["nn"].get_net();
    layer = math.floor(rand.uniform(1, len(nn)));
    neuron = math.floor(rand.uniform(0, len(nn[layer])));
    nn[layer][neuron]["bias"] += rand.uniform(-1, 1);
    return (elem);

################################################################################
##[ MAIN ]######################################################################
################################################################################

##############################
##[ INIT ]####################
##############################
##### INIT MODULES #####
game = awale.c_awale();
gene = ga.c_ga();
##### INIT POPULATION #####
population = [];
for i in range(0, POPULATION):
    new_nn = nn.c_nn();
    new_nn.init(LAYERS);
    population.append({"note": 0, "nn": new_nn});
gene.set_population(population);
gene.set_birth_function(genetic_birth_mean);
gene.add_mutation(genetic_mutation_weight, CHANCE_WEIGHT);
gene.add_mutation(genetic_mutation_weight_2, CHANCE_WEIGHT_2);
gene.add_mutation(genetic_mutation_bias, CHANCE_BIAS);
gene.set_selection_mode(SELECTION_MODE);

##############################
##[ LAUNCH ]##################
##############################
for i in range(0, GENERATION):
    rand.shuffle(population);
    for j in range(0, POPULATION // 2):
        player_a = population[j * 2];
        player_b = population[j * 2 + 1];
        ret = play(game, player_a["nn"], player_b["nn"]);
        score = game.get_score();
        winner = game.get_winner();
        player_a["note"] = score[0];
        player_b["note"] = score[1];
        turn_score = (MAX_TURN - ret) // 2;
        if (winner == 1):
            player_a["note"] += turn_score + (score[0] - score[1]) // 3;
        else:
            player_b["note"] += turn_score + (score[1] - score[0]) // 3;
    gene.select(SELECTION);
    population.sort(reverse=True, key=lambda elem: elem["note"]);
    print_wave(i, population[0]["note"]);

##############################
##[ SAVE ]####################
##############################
best_player = gene.get_best_elements(1)[0]["nn"];
if (len(sys.argv) == 2):
    best_player.export(sys.argv[1]);
    print("element saved into [" + sys.argv[1] + ".nno]");
else:
    file_name = input("If you want to save the best element, please give a file name:");
    best_player.export(file_name);
    print("element saved into [" + file_name + ".nno]");
